/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * Proddata tool main entry point
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#ifdef __ANDROID__
#include <android-base/logging.h>
#define DLOG(x) LOG(x)
#else
#include <glog/logging.h>
#endif
#include <iomanip>
#include <iostream>
#include <string>
#include "proddata.h"
#include "flash_access.h"
#include "userotp_access.h"

static void PrintData(std::vector<uint8_t> &data) {
  int size = data.size();
  for (int i = 0; i < size; i++) {
    std::cout << std::setw(2) << std::setfill('0') << std::hex << static_cast<int>(data[i]);
  }
  std::cout << std::endl;
}

static void usage() {
  std::string mesg = "Usage: proddata write <data>             Write calibration data \n"
                     "       proddata read                     Read calibration data \n"
                     "       proddata read <name e.g DCXO>     Read data field \n";
  std::cerr << mesg;
}

#ifdef __ANDROID__
int main(int argc, char* argv[]) {

  if (argc < 2) {
    usage();
    return -1;
  }

  // TODO(Sagar): Make FlashAccess implemetation and hardcoded device name configurable
  std::unique_ptr<FlashAccess> flash_access(new UserOTPAccess("/dev/mtd/mtd0"));
  Proddata proddata(std::move(flash_access));

  if (!strcmp(argv[1], "write")) {
    if (argv[2] == NULL) {
      std::cerr << "Specify data to be written to OTP" << std::endl;
      return -1;
    } else {
      proddata.Write(argv[2]);
    }
  } else if (!strcmp(argv[1], "read")) {
    std::vector<uint8_t> data;
    if (argv[2] == NULL) {
      data = proddata.Read();
    } else {
      data = proddata.ReadValue(argv[2]);
    }
    PrintData(data);
  } else {
    std::cerr << "Invalid command" << std::endl;
    usage();
    return -1;
  }

  return 0;
}
#else
int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);

  if (argc < 2) {
    usage();
    return -1;
  }

  try {
    // TODO(Sagar): Make FlashAccess implemetation and harcoded device name configurable
    std::unique_ptr<FlashAccess> flash_access(new UserOTPAccess("/dev/mtd1"));
    Proddata proddata(std::move(flash_access));

    if (!strcmp(argv[1], "write")) {
      if (argv[2] == NULL) {
        std::cerr << "Specify data to be written to OTP" << std::endl;
        return -1;
      } else {
        proddata.Write(argv[2]);
      }
    } else if (!strcmp(argv[1], "read")) {
      std::vector<uint8_t> data;
      if (argv[2] == NULL) {
        data = proddata.Read();
      } else {
        data = proddata.ReadValue(argv[2]);
      }
      PrintData(data);
    } else {
      std::cerr << "Invalid command" << std::endl;
      usage();
      return -1;
    }
  } catch (std::runtime_error &e) {
    google::ShutdownGoogleLogging();
    return -1;
  }

  google::ShutdownGoogleLogging();
  return 0;
}
#endif
