#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Select the SoC.
$(call set_soc, imagination, fs1130)

# Add WiFi controller
$(call add_peripheral, imagination, wifi/uccp420)

$(call add_peripheral, imagination, audio/fs1130)
$(call add_peripheral, imagination, light/fs1130)

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 268435456
BOARD_USERDATAIMAGE_PARTITION_SIZE := 134217728
BOARD_CACHEIMAGE_PARTITION_SIZE := 268435456
BOARD_U_BOOT_ENV_SIZE := 0x10000
CREATORCI41_BOARD_GPT_INI := device/imagination/creatorci41/gpt.ini

BOARD_MKBOOTIMG_ARGS := \
    --base 0x80400000 \
    --kernel_offset 0x00000000 \
    --ramdisk_offset 0x02000000

PRODUCT_COPY_FILES += \
    device/imagination/creatorci41/fstab.device:root/fstab.${soc_name} \
    device/imagination/creatorci41/provision-device:provision-device

DEVICE_PACKAGES += \
    peripheral_io.fs1130 \
    proddata

BOARD_SEPOLICY_DIRS += \
    device/imagination/creatorci41/sepolicy

TARGET_BOARD_PLATFORM := fs1130

# Must be defined at the end of the file.
$(call add_device_packages)
