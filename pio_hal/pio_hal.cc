/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <base/logging.h>
#include <hardware/hardware.h>
#include <hardware/peripheral_io.h>

static int register_device(const peripheral_io_module_t* dev __unused,
                           const peripheral_registration_cb_t* callbacks) {
  LOG(INFO) << "register_device creatorci41";

  // If a jumper (JPx) is mentioned in the comment, it must be set correctly to
  // route the SOC pin to the output connector

  // Register GPIO Pins
  callbacks->register_gpio_sysfs("MFIO_22", 22); // RPi[7]  (JP1) / MIK1 AN (JP1)
  callbacks->register_gpio_sysfs("MFIO_14", 14); // RPi[8]  (JP7) / MIK1 RX (JP7)
  callbacks->register_gpio_sysfs("MFIO_75", 75); // RPi[10] (JP8) / MIK1 TX (JP8)
  callbacks->register_gpio_sysfs("MFIO_25", 25); // RPi[11] (JP9) / MIK2 AN (JP9)
  callbacks->register_gpio_sysfs("MFIO_23", 23); // RPi[13]       / MIK1 RST
  callbacks->register_gpio_sysfs("MFIO_24", 24); // RPi[15]       / MIK2 INT (JP11)
  callbacks->register_gpio_sysfs("MFIO_88", 88); // RPi[19] (JP4) / MIK2 MOSI (JP4)
  callbacks->register_gpio_sysfs("MFIO_89", 89); // RPi[21] (JP5) / MIK2 MISO (JP5)
  callbacks->register_gpio_sysfs("MFIO_21", 21); // RPi[22] (JP3) / MIK1 INT (JP3)
  callbacks->register_gpio_sysfs("MFIO_31", 31); // RPi[23] (JP6) / MIK1 CS (JP6)
  callbacks->register_gpio_sysfs("MFIO_28", 28); // RPi[24] (JP2) / MIK1 CS (JP2)
  callbacks->register_gpio_sysfs("MFIO_27", 27); // RPi[29]       / MIK2 RST
  callbacks->register_gpio_sysfs("MFIO_72", 72); // RPi[31]
  callbacks->register_gpio_sysfs("MFIO_80", 80); // RPi[33]
  callbacks->register_gpio_sysfs("MFIO_81", 81); // RPi[35]
  callbacks->register_gpio_sysfs("MFIO_82", 82); // RPi[36]
  callbacks->register_gpio_sysfs("MFIO_83", 83); // RPi[37]
  callbacks->register_gpio_sysfs("MFIO_84", 84); // RPi[38]
  callbacks->register_gpio_sysfs("MFIO_85", 85); // RPi[40]

  callbacks->register_gpio_sysfs("MFIO_73", 73); //                 MIK1 PWM
  callbacks->register_gpio_sysfs("MFIO_74", 74); //                 MIK2 PWM

  // Register the SPI bus
  // For RPi and MIK2 the MOSI (JP4), MISO (JP5) and MCLK (JP3) outputs
  // must be correctly configured in addition to the chip selects mentioned below
  callbacks->register_spi_dev_bus("SPI0", 0, 2); // RPi[24] (JP2) / MIK1 CS (JP2)
  callbacks->register_spi_dev_bus("SPI1", 1, 3); // RPi[26]       / MIK2 CS (JP10)

  // Register the I2C bus
  callbacks->register_i2c_dev_bus("I2C0", 0); // RPi[3,5]         / MIK2 SCL/SDA
  callbacks->register_i2c_dev_bus("I2C1", 1); // RPi[12,16]       / MIK1 SCL/SDA

  // Register sysfs leds
  callbacks->register_led_sysfs("LED1", "marduk:red:user1");
  callbacks->register_led_sysfs("LED2", "marduk:red:user2");
  callbacks->register_led_sysfs("LED3", "marduk:red:user3");
  callbacks->register_led_sysfs("LED4", "marduk:red:user4");
  callbacks->register_led_sysfs("LED5", "marduk:red:user5");
  callbacks->register_led_sysfs("LED6", "marduk:red:user6");
  callbacks->register_led_sysfs("LED7", "marduk:red:user7");

  return 0;
}

static struct hw_module_methods_t hal_module_methods = {};

peripheral_io_module_t HAL_MODULE_INFO_SYM = {
    .common =
        {
            .tag = HARDWARE_MODULE_TAG,
            .module_api_version = 0,
            .hal_api_version = HARDWARE_HAL_API_VERSION,
            .id = PERIPHERAL_IO_HARDWARE_MODULE_ID,
            .name = "periperal IO HAL",
            .author = "The Android Open Source Project",
            .methods = &hal_module_methods,
        },
    .register_devices = register_device,
};
